fiboTest()
{
  
  echo "Test if fibo $1 is correct..."

  ./main $1 # Run l'exe CPP

  result=`head -n 1 ./result.txt` # Get le resultat de l'exe
  
  testResult=`head -n $2 ./ListeFibo.txt | tail -n 1` # Get le resultat attendu

  # Test si le resultat obtenu et equivalent au resultat attendu
  
  if [ $result = $testResult ]
  then
    printf "  Test is OK\n  $result == $testResult"
  else
    printf "  Test is NOK\n  $result != $testResult"
  fi
  printf "\n ###### \n"
}

#####################################

fiboTest "30" 30 # fiboTest(char argCPP, int argBASH)

fiboTest "29" 29
