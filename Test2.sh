varTest=0
intLine=0


fiboTest()
{
  #printf "$varTest\n"
  intLine=$((intLine + 1))
  intArg=$1 # String to
  
  ./main $1 # Run l'exe CPP

  result=`head -n 1 ./result.txt` # Get le resultat de l'exe
  
  testResult=`head -n $intArg ./ListeFibo.txt | tail -n 1` # Get le resultat attendu

  # Test si le resultat obtenu et equivalent au resultat attendu
  
  if [ $result != $testResult ]
  then
    #printf "fibo $1 | $result != $testResult\n"
    echo "TEST FAILED AT TEST $intLine WHERE FIBO = $1" >> errors.log
    varTest=1
  else
    #printf "fibo $1 | $result == $testResult\n"
    varTest=$varTest
  fi

}

#####################################

echo "" > errors.log

fiboTest "30"

fiboTest "29"

fiboTest "28"

fiboTest "27"

if [ $varTest = 0 ]
then
  echo 0
else
  echo 1
fi
