#include <iostream>
#include <fstream>
#include <string>

// Fonction de Fibonacci
int fibonacci(int nb_pre, int nb_post, int nb_display) {
    if (nb_display==0) 
    {
        //cout << nb_pre << endl;
        std::ofstream result("./result.txt"); // Ecriture dans un fichier
        result << nb_pre << std::endl;
        return 0;
    }
    int swap = nb_pre;
    nb_pre = nb_post;
    nb_post = swap + nb_post;
    return fibonacci(nb_pre, nb_post,nb_display-1);
}

// Prog Principal : Appel de la fonction fibonacci()
int main(int argc, char *argv[] ) {
    int valEnter = std::stoi(argv[1]);
    fibonacci(0, 1, valEnter);
    return 0;
}
